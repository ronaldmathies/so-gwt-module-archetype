#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.domain;

/**
 * @author Ronald Mathies
 */
public class Constants {

    /**
     * Persistence unit name.
     */
    public final static String PU = "${applicationName.toLowerCase()}-pu";

}
