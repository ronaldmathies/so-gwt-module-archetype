#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client;

import nl.sodeso.gwt.ui.client.controllers.center.AbstractWelcomeCenterPanel;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

/**
 * @author Ronald Mathies
 */
public class Welcome extends AbstractWelcomeCenterPanel {

    @Override
    public void beforeAddWelcomePanel(Trigger trigger) {
        trigger.fire();
    }

}
