#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client;

import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.link.Link;

import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class WelcomeLink implements Link {

    public static final String TOKEN = "${applicationName.toLowerCase()}-welcome";

    @Override
    public void navigate(String token, Map<String, String> arguments) {
        CenterController.instance().setWidget(new Welcome());
    }

    @Override
    public boolean canProcessToken(String token) {
        return TOKEN.equals(token);
    }

}
