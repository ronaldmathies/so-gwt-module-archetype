#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.server.endpoint.properties;

import ${package}.client.Constants;
import ${package}.client.properties.${applicationName}ClientAppProperties;

import nl.sodeso.gwt.ui.server.endpoint.properties.AbstractPropertiesEndpoint;
import nl.sodeso.gwt.ui.server.util.ApplicationPropertiesContainer;

import javax.servlet.annotation.WebServlet;

/**
 * By default the application has an endpoint for properties consisting of the following settings:
 *
 * <ul>
 *  <li>name: Name of the application (used as the title)</li>
 *  <li>version: Version number of the application.</li>
 *  <li>devkitEnabled: Are the extra development tools available.</li>
 * </ul>
 *
 * If you would like to have more global application settings (not user specific), that don't
 * change during a session, then this is the place to add them. Just add the fields below
 * and fill them using the endpoint.
 *
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.${applicationName.toLowerCase()}-properties"})
public class ${applicationName}PropertiesEndpoint extends AbstractPropertiesEndpoint<${applicationName}ClientAppProperties, ${applicationName}ServerAppProperties> {

    @Override
    public void fillApplicationProperties(${applicationName}ServerAppProperties serverAppProperties, ${applicationName}ClientAppProperties clientAppProperties) {
    }

    @Override
    public Class getClientAppPropertiesClass() {
        return ${applicationName}ClientAppProperties.class;
    }

    @Override
    public ${applicationName}ServerAppProperties getServerAppProperties() {
        return ApplicationPropertiesContainer.instance().getApplicationProperties(Constants.MODULE_ID);
    }

}

