#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.server.listener;

import ${package}.client.Constants;

import nl.sodeso.gwt.ui.server.listener.AbstractPropertiesInitializerContextListener;

import javax.servlet.annotation.WebListener;

/**
 * @author Ronald Mathies
 */
@WebListener
public class ${applicationName}PropertiesInitializerContextListener extends AbstractPropertiesInitializerContextListener {

    @Override
    public Class getServerAppPropertiesClass() {
        return ${applicationName}ServerAppProperties.class;
    }

    @Override
    public String getConfiguration() {
        return "${rootArtifactId}-configuration.properties";
    }

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }
}
