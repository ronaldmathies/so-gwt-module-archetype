#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.server.filter;

import ${package}.domain.Constants;
import nl.sodeso.persistence.hibernate.UnitOfWorkFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * @author Ronald Mathies
 */
@WebFilter(
    urlPatterns = {"/*"},
    initParams = {
        @WebInitParam(name = UnitOfWorkFilter.INIT_PERSISTENCE_UNIT, value = Constants.PU),
        @WebInitParam(name = UnitOfWorkFilter.INIT_EXTRA_URL_PATTERN, value = ".*/${applicationName.toLowerCase()}/.*")
    }
)
public class ${applicationName}UnitOfWorkFilter extends UnitOfWorkFilter {
}