#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client.application;

import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuController;

/**
 * @author Ronald Mathies
 */
public class StartMenu {

    private StartMenu() {}

    public static void init() {
        MenuController.instance().setFullwidth(true);

        MenuController.instance().addMenuItems(
                new MenuItem("menu-item-1", "Menu-Item 1", null),
                new MenuItem("menu-item-2", "Menu-Item 2", null)
                        .addChildren(
                                new MenuItem("sub-item-1", "Sub-Item 1", null),
                                new MenuItem("sub-item-2", "Sub-Item 2", null),
                                new MenuItem("sub-item-3", "Sub-Item 3", null)
                    ),
                new MenuItem("menu-item-3", "Menu-Item 3", null));
    }

}
