#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client;

import com.google.gwt.core.client.GWT;
import ${package}.client.application.StartMenu;

import nl.sodeso.gwt.ui.client.rpc.DefaultRunAsyncCallback;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.link.LinkFactory;


/**
 * The application entry point is the main method of a GWT application, it should handle
 * the boodstrapping of the application.
 *
 * @author Ronald Mathies
 */
public class ${applicationName}EntryPoint extends ModuleEntryPoint {

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }

    @Override
    public String getModuleName() {
        return "${applicationName}";
    }

    @Override
    public Icon getModuleIcon() {
        return Icon.Users;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initModule(final InitFinishedTrigger trigger) {
        GWT.runAsync(new DefaultRunAsyncCallback(){
            @Override
            public void success(){
                StartMenu.init();

                if (LinkFactory.getLink(getWelcomeToken()) == null) {
                    LinkFactory.addLink(new WelcomeLink());
                }

                trigger.fire();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getWelcomeToken() {
        return WelcomeLink.TOKEN;
    }

    /**
     * {@inheritDoc}
     */
    public void activateModule(ActivateFinishedTrigger trigger) {
        trigger.fire();
    }

    /**
     * {@inheritDoc}
     */
    public void suspendModule(SuspendFinishedTrigger trigger) {
        trigger.fire();
    }
}
