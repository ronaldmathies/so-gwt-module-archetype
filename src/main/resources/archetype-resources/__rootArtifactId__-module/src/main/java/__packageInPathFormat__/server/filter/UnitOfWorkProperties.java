#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.server.filter;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;
import ${package}.domain.Constants;

/**
 * @author Ronald Mathies
 */
@Resource(
        domain = Constants.PU
)
@FileResources(
        files = {
                @FileResource(file = "/${applicationName.toLowerCase()}-pu")
        }
)
public class UnitOfWorkProperties extends FileContainer {
    
}
