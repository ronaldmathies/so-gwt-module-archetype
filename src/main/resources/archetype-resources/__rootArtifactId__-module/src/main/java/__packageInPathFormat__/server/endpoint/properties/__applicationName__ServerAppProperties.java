#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.server.endpoint.properties;

import nl.sodeso.gwt.ui.server.util.ServerAppProperties;

/**
 * @author Ronald Mathies
 */
public class ${applicationName}ServerAppProperties extends ServerAppProperties {

    public ${applicationName}ServerAppProperties(String configurationFile) {
        super(configurationFile);
    }
}
