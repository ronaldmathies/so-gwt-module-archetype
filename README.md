Archetype for creating GWT modules that can be used in combination with the so-gwt-desktop-archetype.

GETTING STARTED
===============

1. Create a clone of the reposity.
2. Build the archetype project locally using "clean install"
3. Create a new project and choose the "Create from Archetype" option.
4. Click on the "Add Archetype..." button and enter the following information:

    GroupId: nl.sodeso.gwt
    ArtifactId: so-gwt-module-archetype
    Version: 1.0-SNAPSHOT

5. Now choose the newly created ArcheType from the list and click 'Next'.
6. Enter your GroupId, ArtifactId en Version of the project you want to create, click 'Next'.
7. Now choose your Maven installation.
8. Now add the following 'extra' variables:

    'package': The name of the package structure you would like (for example: nl.sodeso.deploykit.console).
    'applicationName': The name of the application, enter the name in a Class style format, so no spaces (for example: DeployKitConsole).
    'applicationTitle': The working name of the application (for example: DeployKit Console).
    'useHibernate': Only add this variable (with value true) if you would like to use Hibernate.
    'baseModuleName': Name of the module (for example: DeployKitConsole)

9. Click generate.

CHECKLIST
===============

1. Using Hibernate & UnitOfWorkFilter

When you have chosen to use hibernate you will need to modify the persistence-unit-name to give it a
more sensible name (name of the datasource).

You need to change this in the following files:

so-persistence.properties
persistence.xml
web.xml (UnitOfWorkFilter)

2. Enabling / disabling remote server logging

If you would like to enable the ability to transfer client side logging to the server side you
need to enable the remoteLogging servlet in the web.xml.

Also enable gwt.logging.simpleRemoteHandler property in your [applicationName].gwt.xml module file.


KNOWN ISSUES
===============

1. Sometimes IntelliJ doesn't recognize the modules, just close the project and re-open it, choose the option to delete the project en re-import it.
2. Sometimes the source and resource folders are not recognized, right click on them and choose the corresponding folder type from the 'Mark as...' menu.